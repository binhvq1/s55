$(document).ready(function() {
    "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gSelectedMenuStructure = { // biến lưu trữ combo được chọn
      menuName: "",    // S, M, L
      duongKinhCM: 0,
      suonNuong: 0,
      saladGr: 0,
      drink: 0,
      priceVND: 0
    };
    var gSelectedPizzaType = ""; // biến lưu trữ loại pizza được chọn
    var gOrderObj = {}; // biến lưu trữ thông tin order

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    $("#btn-size-S").on("click", function() {
        onBtnSelectSizeClick('S')
    })
    $("#btn-size-M").on("click", function() {
        onBtnSelectSizeClick('M')
    })
    $("#btn-size-L").on("click", function() {
        onBtnSelectSizeClick('L')
    })
    $("#btn-type-seafood").on("click", function() {
        onBtnSelectTypeClick('seafood')
    })
    $("#btn-type-hawaii").on("click", function() {
        onBtnSelectTypeClick('hawaii')
    })
    $("#btn-type-bacon").on("click", function() {
        onBtnSelectTypeClick('bacon')
    })
    $("#btn-send-order").click(onBtnSendOrderClick);
    $("#btn-create-order").click(onBtnCreateOrderClick)

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() { // hàm xử lý sự kiện load trang
    console.log("Chạy hàm xử lý khi load trang");
    // callAjaxApiGetCampaignMessage();
    // callAjaxApiGetMenuData();
    callAjaxApiGetDrinkData();
  }
  function onBtnSelectSizeClick(paramSize) { // hàm chạy khi ấn nút chọn combo size
    if(paramSize == "S") {
      var vSelectedMenuStructure = getSize(paramSize, 20, 2, 200, 2, 150000);
    }
    else if(paramSize == "M") {
      var vSelectedMenuStructure = getSize(paramSize, 25, 4, 300, 3, 200000);
    }
    else if(paramSize == "L") {
      var vSelectedMenuStructure = getSize(paramSize, 30, 8, 500, 4, 250000);
    }
    gSelectedMenuStructure = vSelectedMenuStructure; 
    vSelectedMenuStructure.displayInConsoleLog();
    changeSizeButtonColor(paramSize);
  }
  function onBtnSelectTypeClick(paramType) { // hàm chạy khi ấn nút chọn loại pizza
    gSelectedPizzaType = paramType;
    console.log("%cĐã chọn loại pizza: " + paramType, "color:blue");
    changeTypeButtonColor(paramType);
  }
  function onBtnSendOrderClick() { // hàm chạy khi ấn nút gửi đơn
    var vUserObj = { // khai báo biến cục bộ là đối tượng khách hàng 
      menuDuocChon: "",
      loaiPizza: "",
      loaiNuocUong: "",
      hoVaTen: "",
      email: "",
      dienThoai: "",
      diaChi: "",
      loiNhan: "",
      voucherID: "",
      phaiThanhToanVND: ""
    };
    //bước 1: thu thập thông tin khách hàng
    getUserData(vUserObj);
    console.log("%cGhi tạm thông tin khách hàng ra console", "color:blue");
    console.log(vUserObj);
    //bước 2: kiểm tra thông tin khách hàng
    var vCheckResult = validateUserData(vUserObj);
    //bước 3: hiển thị thông tin nếu hợp lệ
    if (vCheckResult == true) {
      console.log("%cThông tin hợp lệ!", "color:blue");
      displayUserDataToModalOrderDetail(vUserObj);
      $("#modal-order-detail").modal("show");
    }
  }
  function onBtnCreateOrderClick() { //hàm chạy khi ấn nút tạo order
    //B3: tạo request tạo đơn
    callAjaxApiCreateOrder(gOrderObj);
    //B4: xử lý hiển thị
    $("#modal-order-detail").modal("hide");
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  function callAjaxApiGetCampaignMessage() { // hàm gửi request lấy campaign message
    $.ajax({
        url: "http://localhost:8080/campaigns/devcamp-pizza365",
        type: "GET",
        dataType: "html",
        success: function(response) {
            console.log("Thông điệp hôm nay là");
            console.log(response);
            displayCampaignMessage(response);
        },
        error: function(err) {
            alert("Có lỗi khi lấy campaign message! Lỗi là:\n\n" + err.responseText)
        }
    })    
  }
  function callAjaxApiGetMenuData() { // hàm gửi request lấy dữ liệu menu combo
    $.ajax({
        url: "http://localhost:8080/combomenu/devcamp-pizza365",
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log("Dữ liệu Menu là");
            console.log(response);
            loadDataToPricingPanel(response);
        },
        error: function(err) {
            alert("Có lỗi khi lấy menu Data! Lỗi là:\n\n" + err.responseText)
        }
    })    
  }
  function callAjaxApiGetDrinkData() { // hàm gửi request lấy dữ liệu drink
    $.ajax({
        url: "http://localhost:8080/drinks/devcamp-pizza365",
        type: "GET",
        dataType: "json",
        success: function(response) {
            //console.log(response);
            loadDataToSelectDrink(response)
        },
        error: function(err) {
            alert("Có lỗi khi lấy drink Data! Lỗi là:\n\n" + err.responseText)
        }
    })    
  }
  function displayCampaignMessage(paramMessage) {
    $("#campaign-message").html(paramMessage)
  }
  function loadDataToPricingPanel(paramMenuList) { // hàm đổ dữ liệu menu vào bảng giá và thông tin
    for (let bMenu of paramMenuList) {
      var bSize = bMenu.kichCo;
      $("#size-"+bSize+"-duongKinh")
        .html(bMenu.duongKinh);
      $("#size-"+bSize+"-suon")
        .html(bMenu.suon);
      $("#size-"+bSize+"-salad")
        .html(bMenu.salad);
      $("#size-"+bSize+"-soLuongNuoc")
        .html(bMenu.soLuongNuoc);
      $("#size-"+bSize+"-thanhTien")
        .html(new Intl.NumberFormat('vi-VN')
          .format(bMenu.thanhTien));
    }
  }
  function loadDataToSelectDrink(paramDrinkList) { // hàm đổ dữ liệu vào select drink
    for (let bDrink of paramDrinkList) {
      $("#select-drink").append($("<option>").val(bDrink.maNuocUong).text(bDrink.tenNuocUong));
    }
  }
  function getSize(paramSize, paramDuongKinhCM, paramSuonNuong, paramSaladGr, paramDrink, paramPriceVND) {
    var vSelectedMenuStructure = {  //Đối tượng: cỡ pizza được chọn
      menuName: paramSize,   //size = S/M/L
      duongKinhCM: paramDuongKinhCM,
      suonNuong: paramSuonNuong,
      saladGr: paramSaladGr,
      drink: paramDrink,
      priceVND: paramPriceVND,
      displayInConsoleLog() {
        console.log("%cPIZZA SIZE SELECTED - cỡ pizza được chọn.....", "color:blue");
        console.log("Size: " + this.menuName + "; Đường kính: " + this.duongKinhCM + " cm"
                + "; Sườn nướng: " + this.suonNuong + "; Salad: " + this.saladGr + " gram"
                + "; Số lượng drink: " + this.drink + "; Giá tiền: " + this.priceVND + " VND");
      }
    }
    return vSelectedMenuStructure;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
  }
  function changeSizeButtonColor(paramSize) { // hàm đổi màu nút chọn combo size
    if (paramSize === "S") {
        $("#btn-size-S").addClass("btn-success").removeClass("btn-warning");
        $("#btn-size-M").addClass("btn-warning").removeClass("btn-success");
        $("#btn-size-L").addClass("btn-warning").removeClass("btn-success");
    }
    else if (paramSize === "M") { 
        $("#btn-size-M").addClass("btn-success").removeClass("btn-warning");
        $("#btn-size-S").addClass("btn-warning").removeClass("btn-success");
        $("#btn-size-L").addClass("btn-warning").removeClass("btn-success");
    } 
    else if (paramSize === "L") { 
        $("#btn-size-L").addClass("btn-success").removeClass("btn-warning");
        $("#btn-size-M").addClass("btn-warning").removeClass("btn-success");
        $("#btn-size-S").addClass("btn-warning").removeClass("btn-success");
    }
  }
  function changeTypeButtonColor(paramType) { // hàm đổi màu nút chọn loại pizza
    if (paramType === "seafood") {
        $("#btn-type-seafood").addClass("btn-success").removeClass("btn-warning");
        $("#btn-type-hawaii").addClass("btn-warning").removeClass("btn-success");
        $("#btn-type-bacon").addClass("btn-warning").removeClass("btn-success");
    }
    else if (paramType === "hawaii") { 
        $("#btn-type-hawaii").addClass("btn-success").removeClass("btn-warning");
        $("#btn-type-seafood").addClass("btn-warning").removeClass("btn-success");
        $("#btn-type-bacon").addClass("btn-warning").removeClass("btn-success");
    } 
    else if (paramType === "bacon") { 
        $("#btn-type-bacon").addClass("btn-success").removeClass("btn-warning");
        $("#btn-type-hawaii").addClass("btn-warning").removeClass("btn-success");
        $("#btn-type-seafood").addClass("btn-warning").removeClass("btn-success");
    }
  }
  function getUserData(paramUserObj) { // hàm lấy thông tin khách hàng
    paramUserObj.menuDuocChon = gSelectedMenuStructure;
    paramUserObj.loaiPizza = gSelectedPizzaType;
    paramUserObj.loaiNuocUong = $("#select-drink").val();
    paramUserObj.hoVaTen = $("#inp-fullname").val().trim();
    paramUserObj.email = $("#inp-email").val().trim();
    paramUserObj.dienThoai = $("#inp-dien-thoai").val().trim();
    paramUserObj.diaChi = $("#inp-dia-chi").val().trim();
    paramUserObj.loiNhan = $("#inp-message").val().trim();
    paramUserObj.voucherID = $("#inp-voucherID").val().trim();
  }
  function validateUserData(paramUserObj) { // hàm kiểm tra thông tin khách hàng
    var vCheckResult = false;
    if (paramUserObj.menuDuocChon.menuName == "") {
      console.log("%cChưa chọn cỡ pizza", "color:red");
      alert("Chưa chọn cỡ pizza");
    }
    else if (paramUserObj.loaiPizza == "") {
      console.log("%cChưa chọn loại pizza", "color:red");
      alert("Chưa chọn loại pizza");
    }
    else if (paramUserObj.loaiNuocUong == "") {
      console.log("%cChưa chọn loại nước uống", "color:red");
      alert("Chưa chọn loại nước uống");
    }
    else if (paramUserObj.hoVaTen == "") {
      console.log("%cChưa nhập họ và tên", "color:red");
      alert("Chưa nhập họ và tên");
    }
    else if (validateEmail(paramUserObj.email) == true) {
      if (validatePhoneNumber(paramUserObj.dienThoai) == true) {
        if (paramUserObj.diaChi == "") {
          console.log("%cChưa nhập địa chỉ", "color:red");
          alert("Chưa nhập địa chỉ");
        }
        else {
          vCheckResult = true
        }
      }
    }
    return vCheckResult
  }
  function validateEmail(paramEmail) { // hàm kiểm tra email
    var vRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var vCheckResult = false;
    if (paramEmail == "") {
      console.log("%cChưa nhập email", "color:red");
      alert("Chưa nhập email");
    }
    else if (vRegEx.test(paramEmail) == false) {
      console.log("%cEmail không đúng định dạng", "color:red");
      alert("Email không đúng định dạng");
    }
    else {
      vCheckResult = true
    }
    return vCheckResult
  }
  function validatePhoneNumber(paramPhoneNumber) { // hàm kiểm tra số điện thoại
    var vCheckResult = false;
    if (paramPhoneNumber == "") {
      console.log("%cChưa nhập số điện thoại", "color:red");
      alert("Chưa nhập số điện thoại");
    }
    else if (isNaN(paramPhoneNumber)) {    
      console.log("%cSố điện thoại phải là dãy số nguyên dương", "color:red");
      alert("Số điện thoại phải là dãy số nguyên dương");
    }
    else {
      var vChuyenSo = Number(paramPhoneNumber);
      if (!Number.isInteger(vChuyenSo) || vChuyenSo <= 0) { 
        console.log("%cSố điện thoại phải là dãy số nguyên dương", "color:red");
        alert("Số điện thoại phải là dãy số nguyên dương");
      }     
      else {
        vCheckResult = true
      }
    }
    return vCheckResult
  }
  function displayUserDataToModalOrderDetail(paramUserObj) { // hàm hiển thị thông tin người dùng
    var vDiscount = getDiscount(paramUserObj.voucherID); //tìm phần trăn giảm theo voucher ID;  
    paramUserObj.phaiThanhToanVND = paramUserObj.menuDuocChon.priceVND * (1 - vDiscount / 100) //tính số tiền phải thanh toán sau giảm giá
    //load dữ liệu vào modal
    $("#modal-order-fullname").val(paramUserObj.hoVaTen);
    $("#modal-order-dien-thoai").val(paramUserObj.dienThoai);
    $("#modal-order-dia-chi").val(paramUserObj.diaChi);
    $("#modal-order-message").val(paramUserObj.loiNhan);
    $("#modal-order-voucherID").val(paramUserObj.voucherID);
    $("#modal-order-detail-info").html(
        "Xác nhận: " + paramUserObj.hoVaTen + ", số điện thoại: " + paramUserObj.dienThoai + ", địa chỉ: " + paramUserObj.diaChi
      + "\nEmail: " + paramUserObj.email
      + "\nMenu: " + paramUserObj.menuDuocChon.menuName  + ", đường kính: " + paramUserObj.menuDuocChon.duongKinhCM + " cm"
      + ", sườn nướng: " + paramUserObj.menuDuocChon.suonNuong
      + ", loại nước uống: " + $("#select-drink option:selected").text() + ", số lượng nước: " + paramUserObj.menuDuocChon.drink
      + "\nLoại pizza: " + paramUserObj.loaiPizza + ", Giá: " + paramUserObj.menuDuocChon.priceVND + " VND" + ", Mã giảm giá: " + paramUserObj.voucherID
      + "\nPhải thanh toán: " + paramUserObj.phaiThanhToanVND + " VND" + " (giảm giá " + vDiscount + " %)"
    );
    // lưu thông tin order vào biến global
    gOrderObj.kichCo = paramUserObj.menuDuocChon.menuName;
    gOrderObj.duongKinh = paramUserObj.menuDuocChon.duongKinhCM;
    gOrderObj.suon = paramUserObj.menuDuocChon.suonNuong;
    gOrderObj.salad = paramUserObj.menuDuocChon.saladGr;
    gOrderObj.loaiPizza = paramUserObj.loaiPizza;
    gOrderObj.idVourcher = paramUserObj.voucherID;
    gOrderObj.idLoaiNuocUong = paramUserObj.loaiNuocUong;
    gOrderObj.soLuongNuoc = paramUserObj.menuDuocChon.drink;
    gOrderObj.hoTen = paramUserObj.hoVaTen;
    gOrderObj.thanhTien = paramUserObj.menuDuocChon.priceVND;
    gOrderObj.email = paramUserObj.email;
    gOrderObj.soDienThoai = paramUserObj.dienThoai;
    gOrderObj.diaChi = paramUserObj.diaChi;
    gOrderObj.loiNhan = paramUserObj.loiNhan
  }
  function getDiscount(paramVoucherID) { // hàm lấy phần trăm giảm giá
    var vDiscount = 0;
    if (paramVoucherID == "") {
      console.log("Không nhập voucher ID");
    }
    else { // nếu mã giảm giá đã nhập, tạo request lấy giảm giá
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramVoucherID,
            type: "GET",
            dataType: "json",
            success: function (response) {
                console.log(response);
                console.log("Tìm thấy mã voucher. Phần trăm giảm giá là: " + response.phanTramGiamGia);
                vDiscount = response.phanTramGiamGia;
            },
            error: function(err) {
                console.log("Voucher đã nhập không tồn tại. Lỗi là: " + err.responseText);
                alert("Voucher đã nhập không tồn tại");
            }
        })
    }
    return vDiscount
  }
  function callAjaxApiCreateOrder(paramOrderObj) { // hàm tạo request tạo order
    $.ajax({
        async: false,
        url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObj),
        dataType: "json",
        success: function (response) {
            console.log(response);
            showModalResult(response)
        },
        error: function(err) {
            console.log("Tạo đơn thất bại! Lỗi là: " + err.responseText);
            alert("Tạo đơn thất bại! Xem lỗi ở console");
        }
    })
  }
  function showModalResult(paramOrderResponse) {
    $("#modal-result-orderId").val(paramOrderResponse.orderId);
    $("#modal-result").modal("show");
  }
})