package com.task55_30.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
//@RequestMapping("/drinks")
public class CDrinkController {
    
	@GetMapping("/devcamp-pizza365")
    public ArrayList<CDrink> getDrinkList() {
        long date = 1615177934000L;
        ArrayList<CDrink> drinkList = new ArrayList<>();
        CDrink drink1 = new CDrink("TRATAC", "Trà tắc", 10000, null, date, date);
        CDrink drink2 = new CDrink("COCA", "Cocacola", 15000, null, date, date);
        CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000, null, date, date);
        CDrink drink4 = new CDrink("LAVIE", "Lavie", 5000, null, date, date);
        CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, date, date);
        CDrink drink6 = new CDrink("FANTA", "Fanta", 15000, null, date, date);
        drinkList.add(drink1);
        drinkList.add(drink2);
        drinkList.add(drink3);
        drinkList.add(drink4);
        drinkList.add(drink5);
        drinkList.add(drink6);
        return drinkList;
    }
}

