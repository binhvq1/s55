package com.devcamp.restapi;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.time.LocalDate;
import java.time.ZoneId;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
//@RequestMapping("/campaigns")
public class CDailyCampaign {
	@GetMapping("/devcamp-pizza365")
    public String getCampaignMessageList() {
        String message = "";
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE")
            .localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault()).plusDays(0);
        String formattedDay = today.format(dtfVietNam);
        if(formattedDay.equals("Thứ Hai")) {
            message = formattedDay + ": Mua 1 tặng 1!";
        }
        else if(formattedDay.equals("Thứ Ba")) {
            message = formattedDay + ": Tặng tất cả khách hàng một phần bánh ngọt!";
        }
        else if(formattedDay.equals("Thứ Tư")) {
            message = formattedDay + ": Giảm 20% cho tất cả các loại Pizza!";
        }
        else if(formattedDay.equals("Thứ Năm")) {
            message = formattedDay + ": Tặng tất cả khách hàng một đồ uống tự chọn!";
        }
        else if(formattedDay.equals("Thứ Sáu")) {
            message = formattedDay + ": Giảm 50% cho tất cả các loại đồ uống!";
        }
        else if(formattedDay.equals("Thứ Bảy")) {
            message = formattedDay + ": Miễn phí giao hàng cho tất cả hóa đơn!";
        }
        else if(formattedDay.equals("Chủ Nhật")) {
            message = formattedDay + ": Giảm 50.000đ cho mỗi 200.000đ trên hóa đơn!";
        }
        return message;
    }
}

