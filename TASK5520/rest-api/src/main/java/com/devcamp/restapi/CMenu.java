package com.devcamp.restapi;

public class CMenu {
    char kichCo;
    byte duongKinh;
    byte suon;
    short salad;
    byte soLuongNuoc;
    int thanhTien;
    public CMenu(char kichCo, byte duongKinh,
    byte suon, short salad, byte soLuongNuoc, int thanhTien) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuoc = soLuongNuoc;
        this.thanhTien = thanhTien;
    }
    public CMenu() {
    }
    public char getKichCo() {
        return kichCo;
    }
    public void setKichCo(char kichCo) {
        this.kichCo = kichCo;
    }
    public byte getDuongKinh() {
        return duongKinh;
    }
    public void setDuongKinh(byte duongKinh) {
        this.duongKinh = duongKinh;
    }
    public byte getSuon() {
        return suon;
    }
    public void setSuon(byte suon) {
        this.suon = suon;
    }
    public short getSalad() {
        return salad;
    }
    public void setSalad(short salad) {
        this.salad = salad;
    }
    public byte getSoLuongNuoc() {
        return soLuongNuoc;
    }
    public void setSoLuongNuoc(byte soLuongNuoc) {
        this.soLuongNuoc = soLuongNuoc;
    }
    public int getThanhTien() {
        return thanhTien;
    }
    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
}
